//
//  AppDelegate.m
//  TopTapController
//
//  Created by songzhou on 22/11/2017.
//  Copyright © 2017 songzhou. All rights reserved.
//

#import "AppDelegate.h"
#import "JMTopTapViewController.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    self.window.frame = [UIScreen mainScreen].bounds;
    
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    NSMutableArray<__kindof UIViewController*> *vcs = [NSMutableArray new];
    for (int i = 0; i < 4; i++) {
        ViewController *vc = [ViewController new];
        vc.text = @(i).stringValue;
        
        [vcs addObject:vc];
    }
    
    JMTopTapViewController *topTapVC = [[JMTopTapViewController alloc] init];
    topTapVC.viewControllers = vcs;
    topTapVC.topStatus.titles = @[@"待受理",@"待发货",@"运输中",@"已完成"];
    
    [tabBarController setViewControllers:@[topTapVC]];

    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:tabBarController];
    
    self.window.rootViewController = navVC;
    
    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
