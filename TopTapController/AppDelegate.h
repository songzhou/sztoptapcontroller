//
//  AppDelegate.h
//  TopTapController
//
//  Created by songzhou on 22/11/2017.
//  Copyright © 2017 songzhou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

