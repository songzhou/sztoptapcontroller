//
//  main.m
//  TopTapController
//
//  Created by songzhou on 22/11/2017.
//  Copyright © 2017 songzhou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
