//
//  ViewController.m
//  TopTapController
//
//  Created by songzhou on 22/11/2017.
//  Copyright © 2017 songzhou. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic) UILabel *label;

@end

@implementation ViewController

- (void)setText:(NSString *)text {
    _text = text;
    if (_label) {
        _label.text = text;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    _label = [UILabel new];
    [self.view addSubview:_label];
    
    _label.translatesAutoresizingMaskIntoConstraints = NO;
    [NSLayoutConstraint activateConstraints:@[
                                              [_label.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor],
                                              [_label.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor]
                                              ]];
    self.view.backgroundColor = [UIColor whiteColor];
    _label.text = _text;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
