//
//  TotalOrderTopStatus.h
//  JU53
//
//  Created by 何晓文 on 16/1/18.
//  Copyright © 2016年 liuhengping. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^clickTop)(NSInteger index);

@interface JMTopTapViewStatus : UIView

@property (nonatomic, strong) UIView *menuView;
@property (nonatomic, strong) UIView *indicattionView;
@property (nonatomic, strong) UIColor *tinColor;
@property (nonatomic, assign) CGFloat menuHeight;

@property (nonatomic, copy) NSArray<NSString *> *titles;

- (void)didClickOrderSort:(clickTop)block;
- (void)scrollToIndex:(NSInteger)index;

@end
