//
//  JMTopTapViewController.m
//  TopTapController
//
//  Created by songzhou on 22/11/2017.
//  Copyright © 2017 songzhou. All rights reserved.
//

#import "JMTopTapViewController.h"

static const CGFloat statusViewHeight = 35;

@interface JMTopTapViewController () <UIScrollViewDelegate>


@property (nonatomic) UIScrollView *mainScrollView;
@property (nonatomic, readwrite) JMTopTapViewStatus *topStatus;


/**
 current index of view controller
 */
@property (nonatomic) NSInteger currentIndex;

@end

@implementation JMTopTapViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        _currentIndex = 0;
    }
    
    return self;
}

- (void)loadView {
    UIView *container = [UIView new];
    
    [container addSubview:self.mainScrollView];
    [container addSubview:self.topStatus];
    
    self.view = container;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    CGFloat startOriginY = CGRectGetMaxY(self.navigationController.navigationBar.frame);
    CGFloat w = CGRectGetWidth(self.view.bounds);
    CGFloat h = CGRectGetHeight(self.view.bounds);
    
    _topStatus.frame = CGRectMake(0, startOriginY, w, statusViewHeight);
    _mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(_topStatus.frame), w, h - statusViewHeight);
    
    [self updateMainScrollViewContentSize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Getters
- (UIScrollView *)mainScrollView {
    if (!_mainScrollView) {
        _mainScrollView = [[UIScrollView alloc] init];
        
        _mainScrollView.backgroundColor = [UIColor clearColor];    // 开启分页
        _mainScrollView.pagingEnabled = YES;
        // 没有弹簧效果
        _mainScrollView.bounces = NO;
        // 隐藏水平滚动条
        _mainScrollView.showsHorizontalScrollIndicator = YES;
        // 设置代理
        _mainScrollView.delegate = self;
    }
    
    return _mainScrollView;
}

- (JMTopTapViewStatus* )topStatus {
    if (!_topStatus) {
        _topStatus = [[JMTopTapViewStatus alloc] init];

        [_topStatus scrollToIndex:0];
        _topStatus.userInteractionEnabled = YES;
        
        [_topStatus didClickOrderSort:^(NSInteger index) {
            self.currentIndex = index;
            // 1 计算滚动的位置
            CGFloat offsetX = index * self.view.frame.size.width;
            self.mainScrollView.contentOffset = CGPointMake(offsetX, 0);
            
            // 2.给对应位置添加对应子控制器
            [self showVc:index];
            
        }];
    }
 
    return _topStatus;
}

#pragma mark - Setters
- (void)setViewControllers:(NSArray<__kindof UIViewController *> *)viewControllers {
    if (_viewControllers) {
        for (UIViewController *vc in _viewControllers) {
            [vc removeFromParentViewController];
        }
    }
    
    _viewControllers = viewControllers;
    
    for (UIViewController *vc in _viewControllers) {
        [self addChildViewController:vc];
    }

    [self updateMainScrollViewContentSize];

    [self showVc:_currentIndex];
}

#pragma mark -
- (void)showVc:(NSInteger)index {
    if (index >= _viewControllers.count) {
        return;
    }

    UIViewController *vc = _viewControllers[index];
    
    if (vc.isViewLoaded) return;
    
    CGFloat offsetX = index * self.view.frame.size.width;
    [self.mainScrollView addSubview:vc.view];
    vc.view.frame = CGRectMake(offsetX, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)updateMainScrollViewContentSize {
    _mainScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.bounds) * _viewControllers.count, 0);
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    // 计算滚动到哪一页
    NSInteger index = scrollView.contentOffset.x / scrollView.bounds.size.width;
    
    // 1.添加子控制器view
    [self showVc:index];
    
    // 2.让选中的标题居中
    [_topStatus scrollToIndex:index];
}


@end
