//
//  JMTopTapViewController.h
//  TopTapController
//
//  Created by songzhou on 22/11/2017.
//  Copyright © 2017 songzhou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMTopTapViewStatus.h"

@interface JMTopTapViewController : UIViewController

@property (nonatomic, copy) NSArray<__kindof UIViewController *> *viewControllers;
@property (nonatomic, readonly) JMTopTapViewStatus *topStatus;

@end
