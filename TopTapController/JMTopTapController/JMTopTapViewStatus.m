//
//  TotalOrderTopStatus.m
//  JU53
//
//  Created by 何晓文 on 16/1/18.
//  Copyright © 2016年 liuhengping. All rights reserved.
//

#import "JMTopTapViewStatus.h"

@interface JMTopTapViewStatus()
@property (nonatomic, copy)clickTop detailBlock;
@property (nonatomic, strong) UIButton *btn;

@property (nonatomic, copy) NSArray<UIButton *> *buttons;
@property (nonatomic) UIView *line;

@end
#define MenuHeight 44
#define IndicattionViewWidth 55
@implementation JMTopTapViewStatus {
    CGFloat _cenX;
}

- (instancetype)initWithFrame:(CGRect)frame{
	if (self = [super initWithFrame:frame]) {
		self.userInteractionEnabled = YES;
        
		self.menuView = [[UIView alloc] init];
		self.menuView.backgroundColor = [UIColor whiteColor];
		self.menuView.userInteractionEnabled = YES;
		[self addSubview:self.menuView];
		
		self.indicattionView = [[UIView alloc] init];
        CGPoint center = self.indicattionView.center;
        center.x = _cenX;
        self.indicattionView.center = center;
		self.indicattionView.backgroundColor = [UIColor redColor];
        [self.menuView addSubview:self.indicattionView];
        
        
		_line = [[UIView alloc] init];
        _line.backgroundColor = [UIColor blackColor];
		[self addSubview:_line];

	}
	
	return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat w = CGRectGetWidth(self.bounds);
    
    _menuView.frame = CGRectMake(0, 0, w, MenuHeight);
    _indicattionView.frame = CGRectMake(0, MenuHeight  - 2, IndicattionViewWidth, 2);
    _line.frame = CGRectMake(0, MenuHeight - 0.5, w, 0.5);

    // update button frame
    for (int i = 0; i < _buttons.count; i++) {
        UIButton *btn = _buttons[i];
        btn.frame = CGRectMake(i * w / _buttons.count, 0, w / _buttons.count, MenuHeight);
        
        if (i == 0) {
            _cenX = btn.center.x;
        }
    }
    
    // update indicator center
    CGPoint center = self.indicattionView.center;
    center.x = _cenX;
    self.indicattionView.center = center;
    
}

- (void)setTitles:(NSArray<NSString *> *)titles {
    if (_buttons) {
        for (UIButton *btn in _buttons) {
            [btn removeFromSuperview];
        }
    }
    
    _titles = titles;
    
    _cenX = 0;
    NSInteger titleCount = _titles.count;
    NSMutableArray<UIButton *> *newButtons = [NSMutableArray arrayWithCapacity:titles.count];
    for (int i = 0; i < titleCount; i++) {
        UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [btn setTitleColor:[UIColor blackColor]  forState:(UIControlStateNormal)];
        [btn setTitle:_titles[i] forState:(UIControlStateNormal)];
        if (i == 0) {
            [btn setTitleColor:[UIColor redColor] forState:(UIControlStateNormal)];
        } else {
            [btn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
        }
        
        
        [self.menuView addSubview:btn];
        btn.tag = 100+i;
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn addTarget:self action:@selector(scrollToDetail:) forControlEvents:(UIControlEventTouchUpInside)];
        
        [newButtons addObject:btn];
    }
    _buttons = newButtons;

    
}

-(void)didClickOrderSort:(clickTop)block {
	self.detailBlock = block;
	
}
- (void)scrollToIndex:(NSInteger)index{
    UIButton* btn =(UIButton *) [self.menuView viewWithTag:index + 100];
    [self scrollToDetail:btn];
}

/**
 *目录滚动方法
 */
- (void)scrollToDetail:(UIButton *)sender {
	if (![self.btn isEqual:sender]) {

		self.btn.selected = NO;
		[self.btn setTitleColor:[UIColor blackColor]  forState:(UIControlStateNormal)];
		
	}
	[sender setTitleColor:[UIColor redColor] forState:(UIControlStateNormal)];
	sender.selected = YES;
	self.btn = sender;
	
	
	if (self.detailBlock) {
		self.detailBlock(sender.tag - 100);
	}
    
		[UIView animateWithDuration:0.3 animations:^{
            CGPoint center = self.indicattionView.center;
            center.x = sender.center.x;
            self.indicattionView.center = center;
			
		}];

}

@end
